FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-pip
COPY ./ ./app 
WORKDIR ./app 
ENV WORKER_CONFIG="config/local.py"
RUN pip3 install -r requirements.txt
RUN pip3 install timezones
ENTRYPOINT ["python3"]
CMD ["cushy.py"]